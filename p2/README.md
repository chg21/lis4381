> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Cameron Gelin

### Project 2 Requirements:

*Three Parts:*

1. Server-side validation
2. Edit/delete functionality
3. RSS feed

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1; 
2. Screenshots as per below examples; 
3. Link to local lis4381 web app: http://localhost/repos/lis4381/ 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Main Page Carousel:*
![](img/img1.png)


*Screenshots of Table Before & After Edit:*

| Before edit              |  After edit |
:-------------------------:|:-------------------------:
![](img/img2.png)  |  ![](img/img5.png)      


*Screenshots of Invalid Edit:*

| Invalid edit              |  Error message |
:-------------------------:|:-------------------------:
![](img/img3.png)  |  ![](img/img4.png)      


*Screenshots of Deleting Record:*

| Delete prompt              |  Table |
:-------------------------:|:-------------------------:
![](img/img6.png)  |  ![](img/img7.png)      

*RSS Feed:*
![](img/img8.png)



