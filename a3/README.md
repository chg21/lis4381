> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Cameron Gelin

### Assignment 3 Requirements:

*Three Parts:*

1. Database
2. Mobile app
3. Skillsets 4-6

#### README.md file should include the following items:


1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of ERD;
3. Screenshot of running application’s first user interface;
4. Screenshot of running application’s second user interface;
5. Screenshots of 10 records for each table—use select * from each table;
6. Links to the following files:
    a. a3.mwb
    b. a3.sql

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:



*Screenshots of My Event Interface*:

| 1st interface              |  2nd interface         |
:-------------------------:|:-------------------------:
![](img/myevent1.png)  |  ![](img/myevent2.png)      


*Screenshots of ERD*:

|    Pet Store ERD:        |
|:-------------------------:|
|![](img/erd.png)

*Screenshots 10 records*:

|    Customer Table:        |
|:-------------------------:|
|![](img/customertable.png)


|  Pet Table:              |
|:-------------------------:
|    ![](img/pettable.png)


|   Pet Store Table:        |
|:-------------------------:
|  ![](img/petstoretable.png)


*Skillsets 4-6:*

Skillset 4:                   |  Skillset 5:                    |   Skillset 6:
:-------------------------:|:-------------------------:|:-------------------------:
![Skillset 1](img/skillset4.png) |    ![Skillset 2](img/skillset5.png) |    ![Skillest 3](img/skillset6.png)


#### Links to files:

[a3.mwb](https://bitbucket.org/chg21/lis4381/src/master/a3/documents/a3.mwb)

[a3.sql](https://bitbucket.org/chg21/lis4381/src/master/a3/documents/a3.sql)
