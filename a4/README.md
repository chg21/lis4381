> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Cameron Gelin

### Assignment 4 Requirements:

*Three Parts:*

1. Online Portfolio
2. Data validation
3. Skilsets 10-12

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1; 
2. Screenshots as per below examples; 
3. Link to local lis4381 web app: http://localhost/repos/lis4381/ 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of LIS4381 Portal (Main Page):*

|    Main Page:             |
|:-------------------------:|
|![](img/img1.png)

*Screenshots of Validations:*

| Failed Validation              |  Passed Validation |
:-------------------------:|:-------------------------:
![](img/img3.png)  |  ![](img/img2.png)      


*Skillsets 10-12:*

Skillset 10:                   |  Skillset 11:                    |   Skillset 12:
:-------------------------:|:-------------------------:|:-------------------------:
![Skillset 1](img/skillset10.png) |    ![Skillset 2](img/skillset11.png) |    ![Skillest 3](img/skillset12.png)