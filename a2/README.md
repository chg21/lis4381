> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Cameron Gelin

### Assignment 2 Requirements:

*Four Parts:*

1. Create a mobile recipe app using Android Studio.
2. 2 screenshots of running user interfaces
3. Chapter Questions (Chs 3, 4)
4. Skillsets 1-3

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface
* Screenshots of Skillsets 1-3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of Healthy Apps Interface*:

Homepage                    |  Recipe Page
:-------------------------:|:-------------------------:
![](img/screenshot1.png)  |  ![](img/screenshot2.png)


*Skillsets 1-3:*

Skillset 1:                   |  Skillset 2:                    |   Skillset 3:
:-------------------------:|:-------------------------:|:-------------------------:
![Skillset 1](img/skillset1.png) |    ![Skillset 2](img/skillset2.png) |    ![Skillest 3](img/skillset3.png)