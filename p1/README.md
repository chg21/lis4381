> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Cameron Gelin

### Project 1 Requirements:

*Two Parts:*

1. Mobile app
2. Skillsets 7-9

#### README.md file should include the following items:


1. Course title, your name, assignment requirements, as per A1;
3. Screenshot of running application’s first user interface;
4. Screenshot of running application’s second user interface;
5. Screenshots of skillsets;

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:



*Screenshots of Business Card Interface*:

| 1st interface              |  2nd interface         |
:-------------------------:|:-------------------------:
![](img/interface1.png)  |  ![](img/interface2.png)      



*Skillsets 7-9:*

Skillset 7:                   |  Skillset 8:                    |   Skillset 9:
:-------------------------:|:-------------------------:|:-------------------------:
![Skillset 1](img/skillset7.png) |    ![Skillset 2](img/skillset8.png) |    ![Skillest 3](img/skillset9.png)