> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>


# LIS4381 - Mobile Web Application Development

## Cameron Gelin

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/chg21/lis4381/src/master/a1/README.md)
    * Install AMPPS, JDK, and Android Studio
    * Create my First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and lis4381)
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/chg21/lis4381/src/master/a2/README.md)
    * Create a mobile recipe app using Android Studio
    * Provide screenshots of completed app
    * Provide Skillsets 1-3

3. [A3 README.md](https://bitbucket.org/chg21/lis4381/src/master/a3/README.md)
    * Screenshots Pet ERD
    * Screenshots of Mobile app
    * Provide Skillsets 4-6

4. [P1 README.md](https://bitbucket.org/chg21/lis4381/src/master/p1/README.md)
    * Create a mobile business card
    * Provide screenshots of completed app
    * Provide Skillsets 7-9
    
5. [A4 README.md](https://bitbucket.org/chg21/lis4381/src/master/a4/README.md)
    * Create online portfolio
    * Provide screenshots of main page and validation
    * Provide Skillsets 10-12

6. [A5 README.md](https://bitbucket.org/chg21/lis4381/src/master/a5/README.md)
    * Server-side validation
    * Provide screenshots of before/after add
    * Screenshot of failed validation
    * Provide Skillsets 13-15
    
7. [P2 README.md](https://bitbucket.org/chg21/lis4381/src/master/p2/README.md)
    * Server-side edit/delete functionality
    * RSS feed