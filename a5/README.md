> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Cameron Gelin

### Assignment 5 Requirements:

*Three Parts:*

1. Online Portfolio
2. Server-side validation
3. Skilsets 13-15

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1; 
2. Screenshots as per below examples; 
3. Link to local lis4381 web app: http://localhost/repos/lis4381/ 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Pet Store before and after add:*

| Before add             |  After add |
:-------------------------:|:-------------------------:
![](img/img1.png)  |  ![](img/img5.png)      


*Screenshots of Validations:*

| Bad input              |  Valid input |
:-------------------------:|:-------------------------:
![](img/img2.png)  |  ![](img/img4.png)      

*Error Message:*
![](img/img3.png)


*Skillsets 13:*
![Skillset 13](img/skillset13.png)


*Skillset 14:*

| Addition Index             |  Addition Process |
:-------------------------:|:-------------------------:
![](img/ss14i.png)  |  ![](img/ss14p.png)      


| Division Index             |  Division Process |
:-------------------------:|:-------------------------:
![](img/ss14i2.png)  |  ![](img/ss14p2.png)      


*Skillset 15:*

|  Index             |   Process |
:-------------------------:|:-------------------------:
![](img/ss15i.png)  |  ![](img/ss15p.png)      

